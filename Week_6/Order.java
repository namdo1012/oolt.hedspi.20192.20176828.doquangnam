package AimsProject;

import java.util.Date;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders;
    private int qtyOrdered; //Quantity of ordered items
    private DigitalVideoDisc[] itemOrdered = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    private Date dateOrdered;

    public Order() {
        if (nbOrders == MAX_LIMITED_ORDERS) {
            System.out.println("Max limited orders in day");
            return;
        }
        this.setDateOrdered();
        nbOrders++;
    }

    public Date getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered() {
        dateOrdered = new Date();;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }
    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered < MAX_NUMBERS_ORDERED) {
            itemOrdered[qtyOrdered++] = disc;
            System.out.println("The disc has been added");
        } else {
            System.out.println("The order is almost full");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList){
        int idx = 0;
        while(qtyOrdered < MAX_NUMBERS_ORDERED) {
            if (idx < dvdList.length) {
                // Add to orderedList
                itemOrdered[qtyOrdered++] = dvdList[idx++];
                System.out.println("Item \"" + dvdList[idx - 1].getTitle() + "\" added to orderedList");
            } else {
                //All dvdList had added
                return;
            }
        }
        //Full of ordered
        if (qtyOrdered >= MAX_NUMBERS_ORDERED){
                //Print unadded items
                System.out.println("You are full of ordered now!");
                System.out.println("Not added item:");
                for (int i = idx; i < dvdList.length; i++) {
                    System.out.println((i - idx + 1) + "." + dvdList[i].getTitle());
                }
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
        if (qtyOrdered <= 0){
            System.out.println("No item ordered!");
        } else {
            // find index of the item
            int idx = -1;
            for (int i = 0; i <= qtyOrdered; i++){
                if (itemOrdered[i].equals(disc)) {
                    idx = i;
                    break;
                }
            }
            // Check and delete
            if (idx == -1){
                System.out.println("This item isn't in the ordered list");
            } else {
                qtyOrdered--;
                for (int i = idx; i < qtyOrdered; i++) itemOrdered[i] = itemOrdered[i + 1];
                System.out.println("Removed item!");
            }
        }
    }

    //Get total cost
    public float totalCost() {
        float totalCost = 0;
        for (int i = 0; i < qtyOrdered; i++){
            totalCost += itemOrdered[i].getCost();
        }
        return totalCost;
    }

    public void getALuckyItem(){
        // Get random int
        int luckyNum = (int) (qtyOrdered * Math.random());
        System.out.println("You have earned new free item: " + itemOrdered[luckyNum].getTitle());
    }

    // Print Ordered Items
    public void printOrder() {
        System.out.println("*****************Order******************");
        System.out.println("Date: " + dateOrdered);
        System.out.println("Ordered items:");
        for (int i = 0; i < qtyOrdered; i++) {
            System.out.println(i + 1 + ". DVD - " + itemOrdered[i].getTitle() + " - " + itemOrdered[i].getCategory() + " - " + itemOrdered[i].getDirector() + " - " + itemOrdered[i].getLength() + ": " + itemOrdered[i].getCost() + "$");
        }
        System.out.println("Total cost: " + this.totalCost());
        System.out.println("****************************************");
    }
}
