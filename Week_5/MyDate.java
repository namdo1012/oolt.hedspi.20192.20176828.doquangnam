package AimsProject;

import java.time.LocalDate;

public class MyDate {
    private int day, month, year;

    public MyDate(){
        LocalDate currentDate = LocalDate.now();
        this.day = currentDate.getDayOfMonth();
        this.month = currentDate.getMonthValue();
        this.year = currentDate.getYear();
    }

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String inputDate){
        // Do Something
    }

    public static void main(String[] args) {
        MyDate today = new MyDate();
        
        System.out.println("Day " + today.day);
        System.out.println("Month " + today.month);
        System.out.println("Year " + today.year);
    }
}
