
//Example 6: Equation
import javax.swing.JOptionPane;

public class Ex6_Equation {
	public static void main(String args[]) {
		String strNum_a, strNum_b, strNum_c;
		String strNum_a2, strNum_b2, strNum_c2;
		String strSelection;
		String menu = "1. Phuong trinh bac nhat 1 an.\n2. Phuong trinh bac nhat 2 an.\n3.Phuong trinh bac 2.\n4.Thoat chuong trinh.\n";

		do {
			strSelection = JOptionPane.showInputDialog(null, menu, "Menu", JOptionPane.INFORMATION_MESSAGE);
			switch (strSelection) {
			case "1": { // ax + b = 0
				String Format = "Phuong trinh co dang: ax + b = 0\n";
				strNum_a = JOptionPane.showInputDialog(null, Format + "Nhap a: ", "Nhap a",	JOptionPane.INFORMATION_MESSAGE);
				strNum_b = JOptionPane.showInputDialog(null, Format + "Nhap b: ", "Nhap b",	JOptionPane.INFORMATION_MESSAGE);
				Double a = Double.parseDouble(strNum_a);
				Double b = Double.parseDouble(strNum_b);
				double result = -b / a;
				JOptionPane.showMessageDialog(null, "Ket qua: x = " + result, "Ket qua", JOptionPane.INFORMATION_MESSAGE);
			}
				break;
			case "2": {
				String Format = "Phuong trinh co dang:\n     a1x + b1y = c1\n     a2x + b2y = c2\n";
				strNum_a = JOptionPane.showInputDialog(null, Format + "Nhap a: ", "Nhap a",	JOptionPane.INFORMATION_MESSAGE);
				strNum_b = JOptionPane.showInputDialog(null, Format + "Nhap b: ", "Nhap b", JOptionPane.INFORMATION_MESSAGE);
				strNum_c = JOptionPane.showInputDialog(null, Format + "Nhap c: ", "Nhap c",	JOptionPane.INFORMATION_MESSAGE);
				strNum_a2 = JOptionPane.showInputDialog(null, Format + "Nhap a2: ", "Nhap a2", JOptionPane.INFORMATION_MESSAGE);
				strNum_b2 = JOptionPane.showInputDialog(null, Format + "Nhap b2: ", "Nhap b2", JOptionPane.INFORMATION_MESSAGE);
				strNum_c2 = JOptionPane.showInputDialog(null, Format + "Nhap c2: ", "Nhap c2", JOptionPane.INFORMATION_MESSAGE);

				Double a = Double.parseDouble(strNum_a);
				Double b = Double.parseDouble(strNum_b);
				Double c = Double.parseDouble(strNum_c);
				Double a2 = Double.parseDouble(strNum_a2);
				Double b2 = Double.parseDouble(strNum_b2);
				Double c2 = Double.parseDouble(strNum_c2);

				double result_y = (c2 * a - a2 * c) / (b2 * a - a2 * b);
				double result_x = (c - b * result_y) / a;

				JOptionPane.showMessageDialog(null, "Ket qua:\n   x = " + result_x + "\n   y = " + result_y, "Ket qua",
						JOptionPane.INFORMATION_MESSAGE);
			}
				break;
			case "3": {
				String Format = "Phuong trinh co dang: ax^2 + bx + c = 0\n";
				strNum_a = JOptionPane.showInputDialog(null, Format + "Nhap a: ", "Nhap a",	JOptionPane.INFORMATION_MESSAGE);
				strNum_b = JOptionPane.showInputDialog(null, Format + "Nhap b: ", "Nhap b",	JOptionPane.INFORMATION_MESSAGE);
				strNum_c = JOptionPane.showInputDialog(null, Format + "Nhap c: ", "Nhap c",	JOptionPane.INFORMATION_MESSAGE);

				Double a = Double.parseDouble(strNum_a);
				Double b = Double.parseDouble(strNum_b);
				Double c = Double.parseDouble(strNum_c);

				double delta = b * b - 4 * a * c;
				if (delta < 0)
					JOptionPane.showMessageDialog(null, "Phuong trinh vo nghiem", "Vo nghiem", JOptionPane.INFORMATION_MESSAGE);
				else if (delta == 0) {
					double result_x = -b / (2 * a);
					JOptionPane.showMessageDialog(null, "Phuong trinh co nghiem kep: " + result_x, "Nghiem kep",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					double result_x1 = (-b - Math.sqrt(delta)) / (2 * a);
					double result_x2 = (-b + Math.sqrt(delta)) / (2 * a);
					JOptionPane.showMessageDialog(null,
							"Phuong trinh co 2 nghiem:\n   x1 = " + result_x1 + "\n   x2 = " + result_x2, "2 nghiem",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
				break;
			case "4": {
				System.exit(0);
			}
				break;
			default: {
				JOptionPane.showMessageDialog(null, "Sai cu phap! Hay nhap lai.", "Loi",
						JOptionPane.INFORMATION_MESSAGE);
				strSelection = JOptionPane.showInputDialog(null, menu, "Menu", JOptionPane.INFORMATION_MESSAGE);
			}
			}
		} while (strSelection != "4");
	}
}
